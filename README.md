My Food Cards readme file.

This is a very small application created in a few hours for iPhone that shows your balance and last movements when using a food card (something like a credit card) from a company called Menu Pass (in Spain). As the app only makes sense in Spain, the app is only localized for this country.

That company doesn't have a web service so a technique called scraping is used (http://en.wikipedia.org/wiki/Web_scraping)

In order to achieve scraping, a library called hpple is used, here a small tutorial (http://www.raywenderlich.com/14172/how-to-parse-html-on-ios).

For networking, AFNetworking is used. There are 2 requests in serial 

When processing the website, although is not strictly necessary in this case because the website is small, I move the processing to a background task using GCD (Grand Central Dispatch), and after that I move the result again to the main thread. The program uses the MVC pattern so network and scraping is inside the DataMode class.

I use Cocoapods to manage the libraries, so before running the project just run "pod update" and open the workspace file.

There is also an Interstitial and small banner from Google Ads in the app... I have to eat ;)