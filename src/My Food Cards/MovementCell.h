//
//  MovementCell.h
//  My Food Cards
//
//  Created by rrlopez on 14/4/15.
//  Copyright (c) 2015 Ricardo Ruiz López. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MovementCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *placeLabel;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *balanceLabel;
@end
