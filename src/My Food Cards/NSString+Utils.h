//
//  NSString+Trim.h
//  My Food Cards
//
//  Created by rrlopez on 16/4/15.
//  Copyright (c) 2015 Ricardo Ruiz López. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Utils)
-(NSString*) trim;
- (BOOL)containsString:(NSString *)string;
- (BOOL)containsString:(NSString *)string options:(NSStringCompareOptions)options;
@end
