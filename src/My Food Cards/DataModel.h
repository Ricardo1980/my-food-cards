//
//  DataModel.h
//  My Food Cards
//
//  Created by rrlopez on 14/4/15.
//  Copyright (c) 2015 Ricardo Ruiz López. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataModel : NSObject
@property (nonatomic, strong) NSString* balance;
@property (nonatomic, strong) NSArray* movements;
+ (DataModel *) sharedInstance;
-(void) balanceWithCardNumber:(NSString*)cardNumber dni:(NSString*)dni onSuccess:(void(^)(NSString* balance, NSArray* movements))success onFailure:(void(^)(NSError *error))fail;
@end
