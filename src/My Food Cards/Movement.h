//
//  Movement.h
//  My Food Cards
//
//  Created by rrlopez on 14/4/15.
//  Copyright (c) 2015 Ricardo Ruiz López. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Movement : NSObject
@property (nonatomic, strong) NSString* date;
@property (nonatomic, strong) NSString* place;
@property (nonatomic, strong) NSString* city;
@property (nonatomic, strong) NSString* price;
@property (nonatomic, strong) NSString* balance;
@end
