//
//  NSString+Trim.m
//  My Food Cards
//
//  Created by rrlopez on 16/4/15.
//  Copyright (c) 2015 Ricardo Ruiz López. All rights reserved.
//

#import "NSString+Utils.h"

@implementation NSString (Utils)

-(NSString*) trim {
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

- (BOOL)containsString:(NSString *)string options:(NSStringCompareOptions)options {
    NSRange rng = [self rangeOfString:string options:options];
    return rng.location != NSNotFound;
}

- (BOOL)containsString:(NSString *)string {
    return [self containsString:string options:NSCaseInsensitiveSearch];
}

@end
