//
//  DataModel.m
//  My Food Cards
//
//  Created by rrlopez on 14/4/15.
//  Copyright (c) 2015 Ricardo Ruiz López. All rights reserved.
//

#import "DataModel.h"
#import "Movement.h"
#import "TFHpple.h"
#import "AFNetworking.h"
#import "Movement.h"
#import <CocoaLumberjack/CocoaLumberjack.h>
#import "NSString+Utils.h"

@implementation DataModel

static DataModel* _sharedInstance;

+ (DataModel*) sharedInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[DataModel alloc] init];
    });
    return _sharedInstance;
}

-(void) balanceWithCardNumber:(NSString*)cardNumber dni:(NSString*)dni onSuccess:(void(^)(NSString* balance, NSArray* movements))success onFailure:(void(^)(NSError *error))fail {
    
    // menu pass first 4 digits = 5402
    
    // separate card number in 4 groups
    NSMutableArray* groups=[NSMutableArray new];
    for (int i=0; i<4; i++) {
        NSRange range=NSMakeRange(i*4,4);
        NSString* group=[cardNumber substringWithRange:range];
        [groups addObject:group];
    }
    
    // launch first request (just to get a cookie)
    NSString* firstUrlString=[NSString stringWithFormat:@"https://roda.menupass.es/MenuPass/Produccion/scripts/Empleados/LogEmpleado_conTarjeta.php?NroTar1=%@&NroTar2=%@&NroTar3=%@&NroTar4=%@&IDUSUARIO=%@", groups[0], groups[1], groups[2], groups[3], dni];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    securityPolicy.allowInvalidCertificates = YES;
    manager.securityPolicy = securityPolicy;
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:firstUrlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        // second request, to get the balance (an HTML table)
        NSString* secondUrlString=@"https://roda.menupass.es/MenuPass/Produccion/paginas/Empleados/LstConsumosEmpleados.php";
        [manager GET:secondUrlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {

            // process the scrapping in a background thread in order to avoid blocking the main/ui thread
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                NSError* error=[self scrapeWebsite:responseObject];
                
                // the response goes to the main thread
                dispatch_async(dispatch_get_main_queue(), ^() {
                    if (error) {
                        self.balance=nil;
                        self.movements=nil;
                        fail(error);
                    } else {
                        success(self.balance, self.movements);
                    }
                });
            });

        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            fail(error);
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        fail(error);
    }];
    
}

/* Scraping of the website, if success let the results in movements and balance variables, otherwise returns an NSError */
-(NSError*) scrapeWebsite:(NSData*)data {
    
    // scrappe if the website is asking for twitter or not, depending of this decission, the rest of the tables are a bit different
    TFHpple* doc=[[TFHpple alloc] initWithHTMLData:data];
    NSArray *askingTwitterElements=[doc searchWithXPathQuery:@"//html/body/form/table/tr[2]/td[1]/font"];
    TFHppleElement* askingTwitterElement=askingTwitterElements.firstObject;
    BOOL askingTwitter=[askingTwitterElement.text containsString:@"twitter"];
    
    // scrappe the balance
    NSArray *balanceElements  = [doc searchWithXPathQuery:@"//html/body/table[5]/tr[1]/td[2]/font/b"];
    if (askingTwitter) {
        balanceElements  = [doc searchWithXPathQuery:@"//html/body/table[5]/tr[1]/td[2]/font/b"];
    } else {
        balanceElements  = [doc searchWithXPathQuery:@"//html/body/table[6]/tr[1]/td[2]/font/b"];
    }
    TFHppleElement *balanceElement=balanceElements.firstObject;
    if (!balanceElement) {
        return [NSError errorWithDomain:@"world" code:200 userInfo:@{NSLocalizedDescriptionKey:@"Error searching balance."}];
    }
    self.balance=balanceElement.text;

    // scrapping the movements (the first 2 elements are useless)
    NSArray *movementsElements;
    if (askingTwitter) {
        movementsElements  = [doc searchWithXPathQuery:@"//html/body/table[5]"];
    } else {
        movementsElements  = [doc searchWithXPathQuery:@"//html/body/table[6]"];
    }
    
    TFHppleElement* table=movementsElements.firstObject;
    if (!table) {
        DDLogError(@"Table not found.");
        return [NSError errorWithDomain:@"world" code:200 userInfo:@{NSLocalizedDescriptionKey:@"Table not found."}];
    }

    // take only table rows
    NSMutableArray* rows=[NSMutableArray new];
    for (TFHppleElement * e in table.children) {
        if ([e.tagName isEqualToString:@"tr"]) {
            [rows addObject:e];
        }
    }
    
    // remove the first 2 table rows (they are empty rows)
    if (rows.count<=2) {
        DDLogError(@"Table must have 3 or more rows.");
        return [NSError errorWithDomain:@"world" code:200 userInfo:@{NSLocalizedDescriptionKey:@"Table must have 3 or more rows."}];
    }
    [rows removeObjectsInRange:NSMakeRange(0, 2)];
        
    // iterate the rows
    
    NSMutableArray* movements=[NSMutableArray new];
    
    for (TFHppleElement* row in rows) {
        
        // iterate the cells of the rows
        NSMutableArray* cells=[NSMutableArray new];
        for (TFHppleElement * cell in row.children) {
            // take only the cell elements
            if ([cell.tagName isEqualToString:@"td"]) {
                [cells addObject:cell];
            }
        }
        
        // there must be 5 cells per row
        if (cells.count!=5) {
            DDLogError(@"Row must have 5 cells.");
            return [NSError errorWithDomain:@"world" code:200 userInfo:@{NSLocalizedDescriptionKey:@"Row must have 5 cells."}];
        }

        // create the Movement object
        Movement* movement=[Movement new];
        movement.date=((TFHppleElement*)cells[0]).text;
        movement.place=[((TFHppleElement*)cells[1]).text trim];
        movement.city=[((TFHppleElement*)cells[2]).text trim];
        movement.price=((TFHppleElement*)cells[3]).text;
        movement.balance=((TFHppleElement*)cells[4]).text;
        [movements addObject:movement];
    }

    self.movements=[NSArray arrayWithArray:movements];
    
    // everything is ok
    return nil;
}

@end
