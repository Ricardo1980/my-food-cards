//
//  MainViewController.m
//  My Food Cards
//
//  Created by rrlopez on 9/4/15.
//  Copyright (c) 2015 Ricardo Ruiz López. All rights reserved.
//

#import "MainViewController.h"
#import "MBProgressHUD.h"
#import "WCAlertView.h"
#import "MovementsTableViewController.h"
#import "DataModel.h"
#import <CocoaLumberjack/CocoaLumberjack.h>
#import "PDKeychainBindingsController.h"
@import GoogleMobileAds;

@interface MainViewController () <GADInterstitialDelegate>
@property (weak, nonatomic) IBOutlet UITextField *cardNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *dniTextField;
@property (weak, nonatomic) IBOutlet GADBannerView *bannerView;
@property (weak, nonatomic) IBOutlet UISwitch *rememberCredentialsSwitch;
@property (strong, nonatomic) DFPInterstitial* interstitial;
@end

static NSString * const kDniKey=@"kDniKey";
static NSString * const kCardNumberKey=@"kCardNumberKey";
static NSString * const kRemeberCredentialsKey=@"kRemeberCredentialsKey";

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.cardNumberTextField.text=[[PDKeychainBindingsController sharedKeychainBindingsController] stringForKey:kCardNumberKey];
    self.dniTextField.text=[[PDKeychainBindingsController sharedKeychainBindingsController] stringForKey:kDniKey];
    BOOL rememberCredentials=[[NSUserDefaults standardUserDefaults] boolForKey:kRemeberCredentialsKey];
    [self.rememberCredentialsSwitch setOn:rememberCredentials animated:NO];
    [self.cardNumberTextField becomeFirstResponder];
    
    DDLogInfo(@"Google Mobile Ads SDK version: %@", [GADRequest sdkVersion]);
    self.bannerView.adUnitID = @"ca-app-pub-6362237454788936/5452916601";
    self.bannerView.rootViewController = self;
    [self.bannerView loadRequest:[GADRequest request]];
}

- (IBAction)onRememberCredentialsValueChanged:(UISwitch*)sender {
    [[NSUserDefaults standardUserDefaults] setBool:sender.on forKey:kRemeberCredentialsKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)onGetBalanceButtonPressed {
    
    // save or remove dni and card number locally if
    if (self.rememberCredentialsSwitch.selected) {
        [[PDKeychainBindingsController sharedKeychainBindingsController] storeString:self.cardNumberTextField.text forKey:kCardNumberKey];
        [[PDKeychainBindingsController sharedKeychainBindingsController] storeString:self.dniTextField.text forKey:kDniKey];
    } else {
        [[PDKeychainBindingsController sharedKeychainBindingsController] storeString:nil forKey:kCardNumberKey];
        [[PDKeychainBindingsController sharedKeychainBindingsController] storeString:nil forKey:kDniKey];
    }

    // validate card number
    if (self.cardNumberTextField.text.length!=16) {
        [WCAlertView showAlertWithTitle:@"Error" message:@"Invalid card number" customizationBlock:nil completionBlock:^(NSUInteger buttonIndex, WCAlertView *alertView) {
            [self.cardNumberTextField becomeFirstResponder];
        } cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
        return;
    }
    
    // validate dni
    if (self.dniTextField.text.length==0) {
        [WCAlertView showAlertWithTitle:@"Error" message:@"Invalid DNI" customizationBlock:nil completionBlock:^(NSUInteger buttonIndex, WCAlertView *alertView) {
            [self.dniTextField becomeFirstResponder];
        } cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
        return;
    }

    [self.cardNumberTextField resignFirstResponder];
    [self.dniTextField resignFirstResponder];

    // send the network requests to scrape and scrape the website
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[DataModel sharedInstance] balanceWithCardNumber:self.cardNumberTextField.text dni:self.dniTextField.text onSuccess:^(NSString* balance, NSArray* movements) {
        
        DDLogInfo(@"Balance: %@", balance);
        
        // remove data if required by the user when requests are ok
        if (!self.rememberCredentialsSwitch.on) {
            self.cardNumberTextField.text=nil;
            self.dniTextField.text=nil;
        }

        // load an interstitial
        self.interstitial = [[DFPInterstitial alloc] init];
        self.interstitial.adUnitID = @"ca-app-pub-6362237454788936/8266782205";
        self.interstitial.delegate = self;
        [self.interstitial loadRequest:[DFPRequest request]];

    } onFailure:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        DDLogError(@"Error: %@", error);
        [WCAlertView showAlertWithTitle:@"Error" message:error.localizedDescription customizationBlock:nil completionBlock:^(NSUInteger buttonIndex, WCAlertView *alertView) {
            [self.cardNumberTextField becomeFirstResponder];
        } cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
    }];
}

#pragma mark GADInterstitialDelegate

- (void)interstitialDidReceiveAd:(GADInterstitial *)ad {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [ad presentFromRootViewController:self];
}

- (void)interstitial:(GADInterstitial *)ad didFailToReceiveAdWithError:(GADRequestError *)error {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [self performSegueWithIdentifier:NSStringFromClass(MovementsTableViewController.class) sender:self];
}

- (void)interstitialDidDismissScreen:(DFPInterstitial *)interstitial {
    [self performSegueWithIdentifier:NSStringFromClass(MovementsTableViewController.class) sender:self];
}

@end
