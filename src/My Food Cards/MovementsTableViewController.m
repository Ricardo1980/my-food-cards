//
//  MovementsTableViewController.m
//  My Food Cards
//
//  Created by rrlopez on 14/4/15.
//  Copyright (c) 2015 Ricardo Ruiz López. All rights reserved.
//

#import "MovementsTableViewController.h"
#import "MovementCell.h"
#import "Movement.h"
#import "DataModel.h"

@interface MovementsTableViewController ()
@property (weak, nonatomic) IBOutlet UILabel *balanceLabel;
@end

@implementation MovementsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.balanceLabel.text=[DataModel sharedInstance].balance;
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidEnterBackgroundNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }];
}

-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
}

#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [DataModel sharedInstance].movements.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Movement* movement=[DataModel sharedInstance].movements[indexPath.row];
    MovementCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(MovementCell.class) forIndexPath:indexPath];
    cell.placeLabel.text=movement.place;
    cell.cityLabel.text=movement.city;
    cell.dateLabel.text=movement.date;
    cell.priceLabel.text=movement.price;
    cell.balanceLabel.text=movement.balance;
    return cell;
}

@end
